/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:gpx-file
 * @short_description: a single GPX file for reading or writing
 * @stability: Unstable
 * @include: libgpx/gpx-file.h
 *
 * TODO: Write more documentation.
 *
 * Since: 0.1.0
 */

#include <assert.h>
#include <locale.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/xmlreader.h>

#include "gpx-file.h"
#include "gpx-point.h"
#include "gpx-waypoint.h"

/* Private file members. */
struct _GpxFile {
	/* When this reaches 0, the GpxFile is destroyed. */
	unsigned int ref_count;

	/* TODO */
	GpxRoute **routes;
	GpxTrack **tracks;
	unsigned int n_routes;
	unsigned int n_tracks;

	/* Parse state. TODO: Move to a separate struct? */
	GpxRoute *current_route;
	GpxTrack *current_track;
};

/**
 * gpx_file_new:
 *
 * Create a new, empty GPX file. Tracks and routes may be loaded into this
 * structure using gpx_file_load_from_fd(), or new ones may be added using
 * gpx_file_add_route() or gpx_file_add_track(), then the file saved using
 * gpx_file_save_to_fd().
 *
 * If memory allocation fails, %NULL is returned.
 *
 * Return value: (transfer full) (allow-none): newly allocated #GpxFile, or %NULL; unref using gpx_file_unref()
 *
 * Since: 0.1.0
 */
GpxFile *
gpx_file_new (void)
{
	GpxFile *file;

	file = malloc (sizeof (GpxFile));
	if (file == NULL) {
		/* Allocation failed. */
		return NULL;
	}

	/* Initialise all fields. */
	memset(file, 0, sizeof (*file));
	file->ref_count = 1;
	file->routes = NULL;
	file->n_routes = 0;
	file->tracks = NULL;
	file->n_tracks = 0;

	return file;
}

static void _gpx_file_destroy (GpxFile *file) __attribute__((nonnull));

/* Destroy the given file. */
static void
_gpx_file_destroy (GpxFile *file)
{
	free (file);
	/* TODO: Free routes, tracks, current_route, current_track */
}

/**
 * gpx_file_ref:
 * @self: a #GpxFile
 *
 * Increment the reference count of the given #GpxFile. TODO: Is this thread-safe?
 *
 * Since: 0.1.0
 */
void
gpx_file_ref (GpxFile *self)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	self->ref_count++;
}

/**
 * gpx_file_unref:
 * @self: a #GpxFile
 *
 * Decrement the reference count of the given #GpxFile. If the reference count
 * reaches 0, the #GpxFile is deallocated.
 *
 * TODO: Is this thread-safe?
 *
 * Since: 0.1.0
 */
void
gpx_file_unref (GpxFile *self)
{
	assert (self != NULL);
	assert (self->ref_count > 0);

	self->ref_count--;
	if (self->ref_count == 0) {
		/* Destroy the file. */
		_gpx_file_destroy (self);
	}
}

/**
 * gpx_file_get_routes:
 * @self: a #GpxFile
 * @n_routes: (allow-none) (out): return location for the number of routes,
 * or %NULL
 *
 * Get the routes in the given #GpxFile. The returned array of
 * #GpxRoute<!-- -->s is owned by the #GpxFile and must not be modified or
 * freed. If the #GpxFile contains no routes, %NULL is returned.
 *
 * If @n_routes is non-%NULL, it will be set to the number of routes returned by
 * the function.
 *
 * Return value: (transfer none) (allow-none) (array length=n_routes): array of
 * #GpxRoute<!-- -->s in this file, or %NULL
 *
 * Since: 0.1.0
 */
const GpxRoute * const *
gpx_file_get_routes (const GpxFile *self, unsigned int *n_routes)
{
	assert (self != NULL);
	assert (self->ref_count > 0);

	if (n_routes != NULL) {
		*n_routes = self->n_routes;
	}

	return (const GpxRoute * const *) self->routes;
}

/**
 * gpx_file_get_tracks:
 * @self: a #GpxFile
 * @n_tracks: (allow-none) (out): return location for the number of tracks,
 * or %NULL
 *
 * Get the tracks in the given #GpxFile. The returned array of
 * #GpxTrack<!-- -->s is owned by the #GpxFile and must not be modified or
 * freed. If the #GpxFile contains no tracks, %NULL is returned.
 *
 * If @n_tracks is non-%NULL, it will be set to the number of tracks returned by
 * the function.
 *
 * Return value: (transfer none) (allow-none) (array length=n_tracks): array of
 * #GpxTrack<!-- -->s in this file, or %NULL
 *
 * Since: 0.1.0
 */
const GpxTrack * const *
gpx_file_get_tracks (const GpxFile *self, unsigned int *n_tracks)
{
	assert (self != NULL);
	assert (self->ref_count > 0);

	if (n_tracks != NULL) {
		*n_tracks = self->n_tracks;
	}

	return (const GpxTrack * const *) self->tracks;
}

/**
 * gpx_file_add_route:
 * @self: a #GpxFile
 * @route: (transfer none): a #GpxRoute to add to the file
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_file_add_route (GpxFile *self, const GpxRoute *route)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (route != NULL);

	/* TODO */
}

/**
 * gpx_file_add_track:
 * @self: a #GpxFile
 * @track: (transfer none): a #GpxTrack to add to the file
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_file_add_track (GpxFile *self, const GpxTrack *track)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (track != NULL);

	/* TODO */
}

/**
 * gpx_file_get_bounding_box:
 * @self: a #GpxFile
 * @bounding_box: (out caller-allocates): return location for the bounding box
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_file_get_bounding_box (const GpxFile *self, GpxBoundingBox *bounding_box)
{
	unsigned int i;

	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (bounding_box != NULL);

	/* Initialise the bounding box. */
	bounding_box->left = 0.0;
	bounding_box->right = 0.0;
	bounding_box->top = 0.0;
	bounding_box->bottom = 0.0;

	/* Bound the routes. */
	for (i = 0; i < self->n_routes; i++) {
		GpxBoundingBox route_box;
		gpx_route_get_bounding_box (self->routes[i], &route_box);
		gpx_bounding_box_union (bounding_box, &route_box, bounding_box);
	}

	/* Bound the tracks. */
	for (i = 0; i < self->n_tracks; i++) {
		GpxBoundingBox track_box;
		gpx_track_get_bounding_box (self->tracks[i], &track_box);
		gpx_bounding_box_union (bounding_box, &track_box, bounding_box);
	}
}

static void
xml_error_cb (void *user_data, const char *message,
              xmlParserSeverities severity, xmlTextReaderLocatorPtr locator)
{
	GpxFile *self = (GpxFile *) user_data;

	/* TODO */
}

static bool parse_latitude_take_string (xmlChar *latitude_str, double *out)
	__attribute__((nonnull));
static bool parse_longitude_take_string (xmlChar *longitude_str, double *out)
	__attribute__((nonnull));

/* Consumes the given latitude_str and outputs it as a double. If parsing fails
 * or if latitude_str is NULL, FALSE is returned. Otherwise TRUE is returned. */
static bool
parse_latitude_take_string (xmlChar *latitude_str, double *out)
{
	locale_t c_locale;
	char *end_ptr = NULL;
	double result;

	assert (latitude_str != NULL);
	assert (out != NULL);

	/* Set a dummy output. */
	*out = 0.0;

	if (latitude_str == NULL) {
		return false;
	}

	/* Parse the string in a locale-independent manner. */
	c_locale = newlocale (LC_ALL_MASK, "C", 0);
	result = strtod_l ((const char *) latitude_str, &end_ptr, c_locale); 
	freelocale (c_locale);

	/* Consume the latitude string. */
	xmlFree (latitude_str);

	if (*end_ptr != '\0' || result < -90.0 || result > 90.0) {
		/* Error. */
		return false;
	}

	/* Success. */
	*out = result;

	return true;
}

/* Consumes the given longitude_str and outputs it as a double. If parsing fails
 * or if longitude_str is NULL, FALSE is returned. Otherwise TRUE is
 * returned. */
static bool
parse_longitude_take_string (xmlChar *longitude_str, double *out)
{
	locale_t c_locale;
	char *end_ptr = NULL;
	double result;

	assert (longitude_str != NULL);
	assert (out != NULL);

	/* Set a dummy output. */
	*out = 0.0;

	if (longitude_str == NULL) {
		return false;
	}

	/* Parse the string in a locale-independent manner. */
	c_locale = newlocale (LC_ALL_MASK, "C", 0);
	result = strtod_l ((const char *) longitude_str, &end_ptr, c_locale); 
	freelocale (c_locale);

	/* Consume the longitude string. */
	xmlFree (longitude_str);

	if (*end_ptr != '\0' || result < -180.0 || result >= 180.0) {
		/* Error. */
		return false;
	}

	/* Success. */
	*out = result;

	return true;
}

static int xml_strcmp (const xmlChar *xml_string, const char *sane_string)
	__attribute__((nonnull));

/* A sane version of xmlStrcmp() which doesn't require the constant string
 * you're inevitably comparing against to be casted to (const xmlChar *).
 * The only redeeming quality of libxml2 is that it works fairly well. */
static int
xml_strcmp (const xmlChar *xml_string, const char *sane_string)
{
	return xmlStrcmp (xml_string, (const xmlChar *) sane_string);
}

static int parse_wpt_type_element (GpxFile *self, xmlTextReader *reader)
	__attribute__((nonnull));

/* Reference: http://www.topografix.com/GPX/1/1/#type_wptType
 * Behaviour: This function must be called with the reader on the start node of
 * the element which conforms to the wptType type, and will return with the
 * reader on the start node of the next sibling element. Returns the final
 * reader status. */
static int
parse_wpt_type_element (GpxFile *self, xmlTextReader *reader)
{
	int status;
	double latitude, longitude;

	/* Grab the latitude and longitude. */
	if (parse_latitude_take_string (
			xmlTextReaderGetAttribute (reader,
			                           (const xmlChar *) "lat"),
			&latitude) == false) {
		return -1;
	} else if (parse_longitude_take_string (
			xmlTextReaderGetAttribute (reader,
			                           (const xmlChar *) "lon"),
			&longitude) == false) {
		return -1;
	}

	/* Create a waypoint or point. */
	assert (self->current_route != NULL || self->current_track != NULL);
	if (self->current_route != NULL) {
		GpxWaypoint waypoint;

		/* Waypoint on a route. */
		waypoint.latitude = latitude;
		waypoint.longitude = longitude;

		gpx_route_append_waypoint (self->current_route, &waypoint);
	} else {
		GpxPoint point;

		/* Point on a track. */
		point.latitude = latitude;
		point.longitude = longitude;

		gpx_track_append_point (self->current_track, &point);
	}

	/* Parse the child elements. */
	status = xmlTextReaderRead (reader);
	while (status > 0) {
		xmlReaderTypes node_type;

		/* What type of node are we reading at the moment? */
		node_type = xmlTextReaderNodeType (reader);

		if (node_type == XML_READER_TYPE_ELEMENT) {
			const xmlChar *name;

			/* Start of an element.  */
			name = xmlTextReaderConstName (reader);

			if (xml_strcmp (name, "ele") == 0 ||
			    xml_strcmp (name, "time") == 0 ||
			    xml_strcmp (name, "magvar") == 0 ||
			    xml_strcmp (name, "geoidheight") == 0 ||
			    xml_strcmp (name, "name") == 0 ||
			    xml_strcmp (name, "cmt") == 0 ||
			    xml_strcmp (name, "desc") == 0 ||
			    xml_strcmp (name, "src") == 0 ||
			    xml_strcmp (name, "link") == 0 ||
			    xml_strcmp (name, "sym") == 0 ||
			    xml_strcmp (name, "type") == 0 ||
			    xml_strcmp (name, "fix") == 0 ||
			    xml_strcmp (name, "sat") == 0 ||
			    xml_strcmp (name, "hdop") == 0 ||
			    xml_strcmp (name, "vdop") == 0 ||
			    xml_strcmp (name, "pdop") == 0 ||
			    xml_strcmp (name, "ageofdgpsdata") == 0 ||
			    xml_strcmp (name, "dgpsid") == 0 ||
			    xml_strcmp (name, "extensions") == 0) {
				/* None of these elements are currently
				 * supported. FIXME. */
				status = xmlTextReaderNext (reader);
				continue;
			} else {
				/* Unexpected element. */
				/* TODO: Better error handling. */
				fprintf (stderr,
				         "Unexpected ‘%s’ element.\n",
				         name);
				status = xmlTextReaderRead (reader);
				continue;
			}
		} else if (node_type == XML_READER_TYPE_END_ELEMENT) {
			/* End of the wpt type element. */
			return xmlTextReaderNext (reader);
		} else {
			/* Don't expect to encounter any of these states
			 * in the top-level parser. */
			/* TODO: Better error handling. */
			fprintf (stderr, "Unexpected node type: %u.\n",
			         xmlTextReaderNodeType (reader));
			status = xmlTextReaderRead (reader);
			continue;
		}
	}

	return status;
}

static int parse_rte_element (GpxFile *self, xmlTextReader *reader)
	__attribute__((nonnull));

/* Reference: http://www.topografix.com/GPX/1/1/#element_rte
 * Behaviour: This function must be called with the reader on the start node of
 * the <rte> element, and will return with the reader on the start node of the
 * next sibling element. Returns the final reader status. */
static int
parse_rte_element (GpxFile *self, xmlTextReader *reader)
{
	int status;
	GpxRoute *route;

	/* No attributes to parse. Set up a route. */
	route = gpx_route_new ();
	assert (self->current_route == NULL);
	self->current_route = route;

	/* Parse the child elements. */
	status = xmlTextReaderRead (reader);
	while (status > 0) {
		xmlReaderTypes node_type;

		/* What type of node are we reading at the moment? */
		node_type = xmlTextReaderNodeType (reader);

		if (node_type == XML_READER_TYPE_ELEMENT) {
			const xmlChar *name;

			/* Start of an element.  */
			name = xmlTextReaderConstName (reader);

			if (xml_strcmp (name, "name") == 0 ||
			    xml_strcmp (name, "cmt") == 0 ||
			    xml_strcmp (name, "desc") == 0 ||
			    xml_strcmp (name, "src") == 0 ||
			    xml_strcmp (name, "link") == 0 ||
			    xml_strcmp (name, "number") == 0 ||
			    xml_strcmp (name, "type") == 0 ||
			    xml_strcmp (name, "extensions") == 0) {
				/* None of these elements are currently
				 * supported. FIXME. */
				status = xmlTextReaderNext (reader);
				continue;
			} else if (xml_strcmp (name, "rtept") == 0) {
				/* rtept (route point) element. */
				status = parse_wpt_type_element (self, reader);
				continue;
			} else {
				/* Unexpected element. */
				/* TODO: Better error handling. */
				fprintf (stderr,
				         "Unexpected ‘%s’ element.\n",
				         name);
				status = xmlTextReaderRead (reader);
				continue;
			}
		} else if (node_type == XML_READER_TYPE_END_ELEMENT) {
			/* End of the rte element. */
			status = xmlTextReaderNext (reader);
			goto done;
		} else {
			/* Don't expect to encounter any of these states
			 * in the top-level parser. */
			/* TODO: Better error handling. */
			fprintf (stderr, "Unexpected node type: %u.\n",
			         xmlTextReaderNodeType (reader));
			status = xmlTextReaderRead (reader);
			continue;
		}
	}

done:
	/* Tidy up the route state. */
	if (status > 0) {
		gpx_file_add_route (self, route);
	}

	gpx_route_unref (self->current_route);
	self->current_route = NULL;

	return status;
}

static int parse_trkseg_element (GpxFile *self, xmlTextReader *reader)
	__attribute__((nonnull));

/* Reference: http://www.topografix.com/GPX/1/1/#element_trkseg
 * Behaviour: This function must be called with the reader on the start node of
 * the <trkseg> element, and will return with the reader on the start node of
 * the next sibling element. Returns the final reader status. */
static int
parse_trkseg_element (GpxFile *self, xmlTextReader *reader)
{
	int status;

	/* No attributes to parse. */
	/* FIXME: Add support for track segments in the API. */
	assert (self->current_track != NULL);

	/* Parse the child elements. */
	status = xmlTextReaderRead (reader);
	while (status > 0) {
		xmlReaderTypes node_type;

		/* What type of node are we reading at the moment? */
		node_type = xmlTextReaderNodeType (reader);

		if (node_type == XML_READER_TYPE_ELEMENT) {
			const xmlChar *name;

			/* Start of an element.  */
			name = xmlTextReaderConstName (reader);

			if (xml_strcmp (name, "extensions") == 0) {
				/* None of these elements are currently
				 * supported. FIXME. */
				status = xmlTextReaderNext (reader);
				continue;
			} else if (xml_strcmp (name, "trkpt") == 0) {
				/* trkpt (track point) element. */
				status = parse_wpt_type_element (self, reader);
				continue;
			} else {
				/* Unexpected element. */
				/* TODO: Better error handling. */
				fprintf (stderr,
				         "Unexpected ‘%s’ element.\n",
				         name);
				status = xmlTextReaderRead (reader);
				continue;
			}
		} else if (node_type == XML_READER_TYPE_END_ELEMENT) {
			/* End of the trkseg element. */
			status = xmlTextReaderNext (reader);
			goto done;
		} else {
			/* Don't expect to encounter any of these states
			 * in the top-level parser. */
			/* TODO: Better error handling. */
			fprintf (stderr, "Unexpected node type: %u.\n",
			         xmlTextReaderNodeType (reader));
			status = xmlTextReaderRead (reader);
			continue;
		}
	}

done:
	assert (self->current_track != NULL);

	return status;
}

static int parse_trk_element (GpxFile *self, xmlTextReader *reader)
	__attribute__((nonnull));

/* Reference: http://www.topografix.com/GPX/1/1/#element_trk
 * Behaviour: This function must be called with the reader on the start node of
 * the <trk> element, and will return with the reader on the start node of the
 * next sibling element. Returns the final reader status. */
static int
parse_trk_element (GpxFile *self, xmlTextReader *reader)
{
	int status;
	GpxTrack *track;

	/* No attributes to parse. Set up a track. */
	track = gpx_track_new ();
	assert (self->current_track == NULL);
	self->current_track = track;

	/* Parse the child elements. */
	status = xmlTextReaderRead (reader);
	while (status > 0) {
		xmlReaderTypes node_type;

		/* What type of node are we reading at the moment? */
		node_type = xmlTextReaderNodeType (reader);

		if (node_type == XML_READER_TYPE_ELEMENT) {
			const xmlChar *name;

			/* Start of an element.  */
			name = xmlTextReaderConstName (reader);

			if (xml_strcmp (name, "name") == 0 ||
			    xml_strcmp (name, "cmt") == 0 ||
			    xml_strcmp (name, "desc") == 0 ||
			    xml_strcmp (name, "src") == 0 ||
			    xml_strcmp (name, "link") == 0 ||
			    xml_strcmp (name, "number") == 0 ||
			    xml_strcmp (name, "type") == 0 ||
			    xml_strcmp (name, "extensions") == 0) {
				/* None of these elements are currently
				 * supported. FIXME. */
				status = xmlTextReaderNext (reader);
				continue;
			} else if (xml_strcmp (name, "trkseg") == 0) {
				/* trkseg (track segment) element. */
				status = parse_trkseg_element (self, reader);
				continue;
			} else {
				/* Unexpected element. */
				/* TODO: Better error handling. */
				fprintf (stderr,
				         "Unexpected ‘%s’ element.\n",
				         name);
				status = xmlTextReaderRead (reader);
				continue;
			}
		} else if (node_type == XML_READER_TYPE_END_ELEMENT) {
			/* End of the trk element. */
			status = xmlTextReaderNext (reader);
			goto done;
		} else {
			/* Don't expect to encounter any of these states
			 * in the top-level parser. */
			/* TODO: Better error handling. */
			fprintf (stderr, "Unexpected node type: %u.\n",
			         xmlTextReaderNodeType (reader));
			status = xmlTextReaderRead (reader);
			continue;
		}
	}

done:
	/* Tidy up the track state. */
	if (status > 0) {
		gpx_file_add_track (self, track);
	}

	gpx_track_unref (self->current_track);
	self->current_track = NULL;

	return status;
}

static int parse_gpx_element (GpxFile *self, xmlTextReader *reader)
	__attribute__((nonnull));

/* Reference: http://www.topografix.com/GPX/1/1/#element_gpx.
 * Behaviour: This function must be called with the reader on the start node of
 * the <gpx> element, and will return with the reader on the start node of the
 * next sibling element. Returns the final reader status. */
static int
parse_gpx_element (GpxFile *self, xmlTextReader *reader)
{
	xmlChar *version;
	int status;

	/* Bail if this is a duplicate GPX element. */
	/* TODO */

	/* Check the GPX version. */
	version = xmlTextReaderGetAttribute (reader,
	                                     (const xmlChar *) "version");
	if (version != NULL && xml_strcmp (version, "1.1") != 0) {
		/* TODO: Error. */
		status = -1;
		goto done;
	}
	xmlFree (version);

	/* Parse the child elements. */
	status = xmlTextReaderRead (reader);
	while (status > 0) {
		xmlReaderTypes node_type;

		/* What type of node are we reading at the moment? */
		node_type = xmlTextReaderNodeType (reader);

		if (node_type == XML_READER_TYPE_ELEMENT) {
			const xmlChar *name;

			/* Start of an element.  */
			name = xmlTextReaderConstName (reader);

			if (xml_strcmp (name, "metadata") == 0) {
				/* metadata element is currently not
				 * supported. FIXME. */
				status = xmlTextReaderNext (reader);
				continue;
			} else if (xml_strcmp (name, "wpt") == 0) {
				/* wpt (waypoint) element is currently not
				 * supported outside a track or route. */
				status = xmlTextReaderNext (reader);
				continue;
			} else if (xml_strcmp (name, "rte") == 0) {
				/* rte (route) element */
				status = parse_rte_element (self, reader);
				continue;
			} else if (xml_strcmp (name, "trk") == 0) {
				/* trk (track) element */
				status = parse_trk_element (self, reader);
				continue;
			} else if (xml_strcmp (name, "extensions") == 0) {
				/* extensions element is currently not
				 * supported. FIXME */
				status = xmlTextReaderNext (reader);
				continue;
			} else {
				/* Unexpected element. */
				/* TODO: Better error handling. */
				fprintf (stderr,
				         "Unexpected ‘%s’ element.\n",
				         name);
				status = xmlTextReaderRead (reader);
				continue;
			}
		} else if (node_type == XML_READER_TYPE_END_ELEMENT) {
			/* End of the gpx element. */
			status = xmlTextReaderNext (reader);
			goto done;
		} else {
			/* Don't expect to encounter any of these states
			 * in the top-level parser. */
			/* TODO: Better error handling. */
			fprintf (stderr, "Unexpected node type: %u.\n",
			         xmlTextReaderNodeType (reader));
			status = xmlTextReaderRead (reader);
			continue;
		}
	}

done:
	/* Clean up parse state. */
	assert (self->current_route == NULL);
	assert (self->current_track == NULL);

	return status;
}

static void parse_gpx_file (GpxFile *self, xmlTextReader *reader)
	__attribute__((nonnull));

static void
parse_gpx_file (GpxFile *self, xmlTextReader *reader)
{
	int status;

	/* Set up the reader. Note that validation is deliberately turned off;
	 * parsing should be as permissive as possible to handle malformed
	 * files. For this reason, namespaces are entirely ignored. */
	xmlTextReaderSetErrorHandler (reader, xml_error_cb, self);

	/* Parse stuff. */
	status = xmlTextReaderRead (reader);
	while (status > 0) {
		xmlReaderTypes node_type;

		/* What type of node are we reading at the moment? */
		node_type = xmlTextReaderNodeType (reader);

		if (node_type == XML_READER_TYPE_ELEMENT) {
			const xmlChar *name;

			/* Start of an element.  */
			name = xmlTextReaderConstName (reader);

			if (xml_strcmp (name, "gpx") == 0) {
				/* gpx element. */
				status = parse_gpx_element (self, reader);
				continue;
			} else {
				/* Unexpected element. */
				/* TODO: Better error handling. */
				fprintf (stderr,
				         "Unexpected ‘%s’ element.\n",
				         name);
				status = xmlTextReaderRead (reader);
				continue;
			}
		} else {
			/* Don't expect to encounter any of these states
			 * in the top-level parser. */
			/* TODO: Better error handling. */
			fprintf (stderr, "Unexpected node type: %u.\n",
			         xmlTextReaderNodeType (reader));
			status = xmlTextReaderRead (reader);
			continue;
		}
	}

	/* TODO: Handle final parse status. */
}

/**
 * gpx_file_load_from_fd:
 * @self: a #GpxFile
 * @fd: the open file descriptor to load from
 *
 * TODO
 * TODO: Note: @fd is not closed when this function returns
 *
 * Since: 0.1.0
 */
void
gpx_file_load_from_fd (GpxFile *self, int fd)
{
	xmlTextReader *reader;

	assert (self != NULL);
	assert (self->ref_count > 0);

	/* Create a new XML reader and parse the document. */
	reader = xmlReaderForFd (fd, NULL, NULL, 0);
	if (reader == NULL) {
		/* TODO: Error. */
	}

	parse_gpx_file (self, reader);

	xmlFreeTextReader (reader);
}

/**
 * gpx_file_load_from_data:
 * @self: a #GpxFile
 * @data: the GPX XML data to load
 * @len: length of @data, or <code class="literal">-1</code> if unknown
 *
 * TODO
 * TODO: gpx_file_load_from_fd() is recommended instead because it will do
 * streaming parsing.
 *
 * Since: 0.1.0
 */
void
gpx_file_load_from_data (GpxFile *self, const char *data, ssize_t len)
{
	xmlTextReader *reader;

	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (data != NULL);
	assert (len >= -1);

	/* Create a new XML reader and parse the document. */
	if (len == -1) {
		len = strlen (data);
	}

	reader = xmlReaderForMemory (data, len, NULL, NULL, 0);
	if (reader == NULL) {
		/* TODO: Error. */
	}

	parse_gpx_file (self, reader);

	xmlFreeTextReader (reader);	
}

/**
 * gpx_file_save_to_fd:
 * @self: a #GpxFile
 * @fd: TODO
 * @file_format: TODO
 * @bounding_box: TODO
 * @zoom_level: TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_file_save_to_fd (GpxFile *self, int fd, GpxFileFormat file_format,
                     const GpxBoundingBox *bounding_box,
                     GpxZoomLevel zoom_level)
{
	/* TODO */
}

/**
 * gpx_file_save_to_data:
 * @self: a #GpxFile
 * @len: TODO
 * @file_format: TODO
 * @bounding_box: TODO
 * @zoom_level: TODO
 *
 * TODO
 *
 * Return value: (transfer full): TODO
 *
 * Since: 0.1.0
 */
char *
gpx_file_save_to_data (GpxFile *self, size_t *len, GpxFileFormat file_format,
                       const GpxBoundingBox *bounding_box,
                       GpxZoomLevel zoom_level)
{
	/* TODO */
	return NULL;
}
