/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:gpx-bounding-box
 * @short_description: a rectangular bounding box
 * @stability: Unstable
 * @include: libgpx/gpx-bounding-box.h
 *
 * TODO: Write more documentation.
 *
 * Since: 0.1.0
 */

#include <assert.h>
#include <stddef.h>

#include "gpx-bounding-box.h"

/**
 * gpx_bounding_box_union:
 * @a: TODO
 * @b: TODO
 * @out: (out caller-allocates): TODO
 *
 * TODO: Note, all of @a, @b and @out may alias
 *
 * Since: 0.1.0
 */
void
gpx_bounding_box_union (const GpxBoundingBox *a, const GpxBoundingBox *b,
                        GpxBoundingBox *out)
{
	assert (a != NULL);
	assert (b != NULL);
	assert (out != NULL);

	out->left = (a->left < b->left) ? a->left : b->left;
	out->right = (a->right > b->right) ? a->right : b->right;
	out->top = (a->top < b->top) ? a->top : b->top;
	out->bottom = (a->bottom > b->bottom) ? a->bottom : b->bottom;
}
