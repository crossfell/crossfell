/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPX_ROUTE_H
#define GPX_ROUTE_H

#include <libgpx/gpx-bounding-box.h>
#include <libgpx/gpx-waypoint.h>

#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * GpxRoute:
 *
 * All the fields in the #GpxRoute structure are private and should never be
 * accessed directly.
 * TODO
 *
 * Since: 0.1.0
 */
typedef struct _GpxRoute GpxRoute;

GpxRoute *gpx_route_new (void)
	__attribute__((warn_unused_result)) __attribute__((__malloc__));
void gpx_route_ref (GpxRoute *self) __attribute__((nonnull));
void gpx_route_unref (GpxRoute *self) __attribute__((nonnull));

void gpx_route_get_bounding_box (const GpxRoute *self,
                                 GpxBoundingBox *bounding_box)
	__attribute__((nonnull));

void gpx_route_append_waypoint (GpxRoute *self, const GpxWaypoint *waypoint)
	__attribute__((nonnull));

const GpxWaypoint * const *gpx_route_get_waypoints (const GpxRoute *self,
                                                    unsigned int *n_waypoints)
	__attribute__((nonnull));

#ifdef  __cplusplus
}
#endif /* __cplusplus */

#endif /* !GPX_ROUTE_H */
