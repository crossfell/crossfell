/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPX_H
#define GPX_H

/* Core files */
#include <libgpx/gpx-bounding-box.h>
#include <libgpx/gpx-file.h>
#include <libgpx/gpx-point.h>
#include <libgpx/gpx-route.h>
#include <libgpx/gpx-track.h>
#include <libgpx/gpx-version.h>
#include <libgpx/gpx-waypoint.h>

#endif /* !GPX_H */
