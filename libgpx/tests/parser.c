/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gpx.h"

static int
test_simple_gpx_file (void)
{
	GpxFile *file;

	file = gpx_file_new ();
	gpx_file_load_from_data (file,
		"<?xml version='1.0' encoding='UTF-8' standalone='no' ?>"
		"<gpx xmlns='http://www.topografix.com/GPX/1/1' "
		     "xmlns:gpxx='http://www.garmin.com/xmlschemas/GpxExtensions/v3' "
		     "xmlns:gpxtpx='http://www.garmin.com/xmlschemas/TrackPointExtension/v1' "
		     "creator='Oregon 400t' version='1.1' "
		     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' "
		     "xsi:schemaLocation='http://www.topografix.com/GPX/1/1 "
		                         "http://www.topografix.com/GPX/1/1/gpx.xsd "
		                         "http://www.garmin.com/xmlschemas/GpxExtensions/v3 "
		                         "http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd "
		                         "http://www.garmin.com/xmlschemas/TrackPointExtension/v1 "
		                         "http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd'>"
			"<metadata>"
				"<link href='http://www.garmin.com'>"
					"<text>Garmin International</text>"
				"</link>"
				"<time>2009-10-17T22:58:43Z</time>"
			"</metadata>"
			"<trk>"
				"<name>Example GPX Document</name>"
				"<trkseg>"
					"<trkpt lat='47.644548' lon='-122.326897'>"
						"<ele>4.46</ele>"
						"<time>2009-10-17T18:37:26Z</time>"
					"</trkpt>"
					"<trkpt lat='47.644548' lon='-122.326897'>"
						"<ele>4.94</ele>"
						"<time>2009-10-17T18:37:31Z</time>"
					"</trkpt>"
					"<trkpt lat='47.644548' lon='-122.326897'>"
						"<ele>6.87</ele>"
						"<time>2009-10-17T18:37:34Z</time>"
					"</trkpt>"
				"</trkseg>"
			"</trk>"
		"</gpx>", -1);
	gpx_file_unref (file);

	return 0;
}

int
main (int argc, char *argv[])
{
	int retval = 0;

	retval += test_simple_gpx_file ();

	return retval;
}
