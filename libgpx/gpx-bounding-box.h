/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPX_BOUNDING_BOX_H
#define GPX_BOUNDING_BOX_H

#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * GpxBoundingBox:
 * @left: TODO
 * @right: TODO
 * @top: TODO
 * @bottom: TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
typedef struct {
	double left;
	double right;
	double top;
	double bottom;
} GpxBoundingBox;

void gpx_bounding_box_union (const GpxBoundingBox *a, const GpxBoundingBox *b,
                             GpxBoundingBox *out) __attribute__((nonnull));

#ifdef  __cplusplus
}
#endif /* __cplusplus */

#endif /* !GPX_BOUNDING_BOX_H */
