/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPX_FILE_H
#define GPX_FILE_H

#include <unistd.h> /* ssize_t */

#include <libgpx/gpx-bounding-box.h>
#include <libgpx/gpx-route.h>
#include <libgpx/gpx-track.h>

#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * GpxFileFormat:
 * @GPX_FORMAT_GPX_1_1: GPX XML format, version 1.1
 *
 * TODO
 *
 * Since: 0.1.0
 */
typedef enum {
	GPX_FORMAT_GPX_1_1,
} GpxFileFormat;

/**
 * GpxZoomLevel:
 *
 * TODO
 *
 * Since: 0.1.0
 */
typedef unsigned int GpxZoomLevel;

/**
 * GpxFile:
 *
 * All the fields in the #GpxFile structure are private and should never be
 * accessed directly.
 * TODO
 *
 * Since: 0.1.0
 */
typedef struct _GpxFile GpxFile;

GpxFile *gpx_file_new (void)
	__attribute__((warn_unused_result)) __attribute__((__malloc__));
void gpx_file_ref (GpxFile *self) __attribute__((nonnull));
void gpx_file_unref (GpxFile *self) __attribute__((nonnull));

const GpxRoute * const *gpx_file_get_routes (const GpxFile *self,
                                             unsigned int *n_routes)
	__attribute__((nonnull (1)));
const GpxTrack * const *gpx_file_get_tracks (const GpxFile *self,
                                             unsigned int *n_tracks)
	__attribute__((nonnull (1)));

void gpx_file_add_route (GpxFile *self, const GpxRoute *route);
void gpx_file_add_track (GpxFile *self, const GpxTrack *track);

void gpx_file_get_bounding_box (const GpxFile *self,
                                GpxBoundingBox *bounding_box)
	__attribute__((nonnull));

void gpx_file_load_from_fd (GpxFile *self, int fd) __attribute__((nonnull (1)));
void gpx_file_load_from_data (GpxFile *self, const char *data, ssize_t len)
	__attribute__((nonnull (1, 2)));

void gpx_file_save_to_fd (GpxFile *self, int fd, GpxFileFormat file_format,
                          const GpxBoundingBox *bounding_box,
                          GpxZoomLevel zoom_level)
	__attribute__((nonnull (1)));
char *gpx_file_save_to_data (GpxFile *self, size_t *len,
                             GpxFileFormat file_format,
                             const GpxBoundingBox *bounding_box,
                             GpxZoomLevel zoom_level)
	__attribute__((nonnull (1)));

/* TODO: File metadata. */

#ifdef  __cplusplus
}
#endif /* __cplusplus */

#endif /* !GPX_FILE_H */
