/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:gpx-track
 * @short_description: a GPX track
 * @stability: Unstable
 * @include: libgpx/gpx-track.h
 *
 * TODO: Write more documentation. Describe differences between routes and
 * tracks.
 *
 * Since: 0.1.0
 */

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "gpx-point.h"
#include "gpx-track.h"

/* Private track members. */
struct _GpxTrack {
	/* When this reaches 0, the GpxTrack is destroyed. */
	unsigned int ref_count;

	/* TODO */
};

/**
 * gpx_track_new:
 *
 * Create a new, empty GPX track.
 *
 * If memory allocation fails, %NULL is returned.
 *
 * Return value: (transfer full) (allow-none): newly allocated #GpxTrack,
 * or %NULL; unref using gpx_track_unref()
 *
 * Since: 0.1.0
 */
GpxTrack *
gpx_track_new (void)
{
	GpxTrack *track;

	track = malloc (sizeof (GpxTrack));
	if (track == NULL) {
		/* Allocation failed. */
		return NULL;
	}

	/* Initialise all fields. */
	memset(track, 0, sizeof (*track));
	track->ref_count = 1;

	return track;
}

static void _gpx_track_destroy (GpxTrack *track) __attribute__((nonnull));

/* Destroy the given track. */
static void
_gpx_track_destroy (GpxTrack *track)
{
	free (track);
}

/**
 * gpx_track_ref:
 * @self: a #GpxTrack
 *
 * Increment the reference count of the given #GpxTrack.
 * TODO: Is this thread-safe?
 *
 * Since: 0.1.0
 */
void
gpx_track_ref (GpxTrack *self)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	self->ref_count++;
}

/**
 * gpx_track_unref:
 * @self: a #GpxTrack
 *
 * Decrement the reference count of the given #GpxTrack. If the reference count
 * reaches 0, the #GpxTrack is deallocated.
 *
 * TODO: Is this thread-safe?
 *
 * Since: 0.1.0
 */
void
gpx_track_unref (GpxTrack *self)
{
	assert (self != NULL);
	assert (self->ref_count > 0);

	self->ref_count--;
	if (self->ref_count == 0) {
		/* Destroy the track. */
		_gpx_track_destroy (self);
	}
}

/**
 * gpx_track_get_bounding_box:
 * @self: a #GpxTrack
 * @bounding_box: (out caller-allocates): return location TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_track_get_bounding_box (const GpxTrack *self, GpxBoundingBox *bounding_box)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (bounding_box != NULL);

	/* TODO */
	bounding_box->left = 0.0;
	bounding_box->right = 0.0;
	bounding_box->top = 0.0;
	bounding_box->bottom = 0.0;
}


/**
 * gpx_track_append_point:
 * @self: a #GpxTrack
 * @point: TODO
 *
 * TODO
 *
 * Since: 0.1.0
 */
void
gpx_track_append_point (GpxTrack *self, const GpxPoint *point)
{
	assert (self != NULL);
	assert (self->ref_count > 0);
	assert (point != NULL);

	/* TODO */
}
