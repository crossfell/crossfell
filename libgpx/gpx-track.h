/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * libgpx
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * libgpx is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libgpx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libgpx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPX_TRACK_H
#define GPX_TRACK_H

#include <libgpx/gpx-bounding-box.h>
#include <libgpx/gpx-point.h>

#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * GpxTrack:
 *
 * All the fields in the #GpxTrack structure are private and should never be
 * accessed directly.
 * TODO
 *
 * Since: 0.1.0
 */
typedef struct _GpxTrack GpxTrack;

GpxTrack *gpx_track_new (void)
	__attribute__((warn_unused_result)) __attribute__((__malloc__));
void gpx_track_ref (GpxTrack *self) __attribute__((nonnull));
void gpx_track_unref (GpxTrack *self) __attribute__((nonnull));

void gpx_track_get_bounding_box (const GpxTrack *self,
                                 GpxBoundingBox *bounding_box)
	__attribute__((nonnull));

void gpx_track_append_point (GpxTrack *self, const GpxPoint *point)
	__attribute__((nonnull));

#ifdef  __cplusplus
}
#endif /* __cplusplus */

#endif /* !GPX_TRACK_H */
