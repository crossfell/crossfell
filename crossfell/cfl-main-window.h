/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CFL_MAIN_WINDOW_H
#define CFL_MAIN_WINDOW_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CFL_TYPE_MAIN_WINDOW		(cfl_main_window_get_type ())
#define CFL_MAIN_WINDOW(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), CFL_TYPE_MAIN_WINDOW, CflMainWindow))
#define CFL_MAIN_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), CFL_TYPE_MAIN_WINDOW, CflMainWindowClass))
#define CFL_IS_MAIN_WINDOW(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), CFL_TYPE_MAIN_WINDOW))
#define CFL_IS_MAIN_WINDOW_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), CFL_TYPE_MAIN_WINDOW))
#define CFL_MAIN_WINDOW_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), CFL_TYPE_MAIN_WINDOW, CflMainWindowClass))

typedef struct _CflMainWindowPrivate	CflMainWindowPrivate;

/**
 * CflMainWindow:
 *
 * All the fields in the #CflMainWindow structure are private and should never be accessed directly.
 *
 * Since: 0.1.0
 */
typedef struct {
	/*< private >*/
	GtkApplicationWindow parent;
	CflMainWindowPrivate *priv;
} CflMainWindow;

/**
 * CflMainWindowClass:
 *
 * All of the fields in the #CflMainWindowClass structure are private and should never be accessed directly.
 *
 * Since: 0.1.0
 */
typedef struct {
	/*< private >*/
	GtkApplicationWindowClass parent;
} CflMainWindowClass;

GType cfl_main_window_get_type (void) G_GNUC_CONST;

CflMainWindow *cfl_main_window_new (GtkApplication *application) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;

void cfl_main_window_new_trip (CflMainWindow *self);
void cfl_main_window_open_trip (CflMainWindow *self, const gchar *id);

G_END_DECLS

#endif /* !CFL_MAIN_WINDOW_H */
