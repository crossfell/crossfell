/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CFL_MAP_H
#define CFL_MAP_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CFL_TYPE_MAP		(cfl_map_get_type ())
#define CFL_MAP(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), CFL_TYPE_MAP, CflMap))
#define CFL_MAP_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), CFL_TYPE_MAP, CflMapClass))
#define CFL_IS_MAP(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), CFL_TYPE_MAP))
#define CFL_IS_MAP_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), CFL_TYPE_MAP))
#define CFL_MAP_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), CFL_TYPE_MAP, CflMapClass))

typedef struct _CflMapPrivate	CflMapPrivate;

/**
 * CflMap:
 *
 * All the fields in the #CflMap structure are private and should never be accessed directly.
 *
 * Since: 0.1.0
 */
typedef struct {
	/*< private >*/
	GtkChamplainEmbed parent;
	CflMapPrivate *priv;
} CflMap;

/**
 * CflMapClass:
 *
 * All of the fields in the #CflMapClass structure are private and should never be accessed directly.
 *
 * Since: 0.1.0
 */
typedef struct {
	/*< private >*/
	GtkChamplainEmbedClass parent;
} CflMapClass;

GType cfl_map_get_type (void) G_GNUC_CONST;

CflMap *cfl_map_new (void) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS

#endif /* !CFL_MAP_H */
