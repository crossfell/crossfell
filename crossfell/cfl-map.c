/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:cfl-map
 * @short_description: wrapper around #GtkChamplainEmbed
 * @stability: Unstable
 * @include: crossfell/cfl-map.h
 *
 * Main map class. TODO: More documentation.
 *
 * Since: 0.1.0
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <champlain-gtk/champlain-gtk.h>

#include "cfl-map.h"
#include "cfl-map-gpx-layer.h"

static void cfl_map_dispose (GObject *object);

struct _CflMapPrivate {
	CflMapGpxLayer *gpx_layer;
};

G_DEFINE_TYPE (CflMap, cfl_map, GTK_CHAMPLAIN_TYPE_EMBED)

static void
cfl_map_class_init (CflMapClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (CflMapPrivate));

	gobject_class->dispose = cfl_map_dispose;
}

static void
cfl_map_init (CflMap *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, CFL_TYPE_MAP, CflMapPrivate);

	/* Add a GPX layer. */
	self->priv->gpx_layer = cfl_map_gpx_layer_new ();
	champlain_view_add_layer (gtk_champlain_embed_get_view (GTK_CHAMPLAIN_EMBED (self)),
	                          CHAMPLAIN_LAYER (self->priv->gpx_layer));
}

static void
cfl_map_dispose (GObject *object)
{
	CflMapPrivate *priv = CFL_MAP (object)->priv;

	g_clear_object (&priv->gpx_layer);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (cfl_map_parent_class)->dispose (object);
}

/**
 * cfl_map_new:
 *
 * Creates a new #CflMap with default properties.
 *
 * Return value: (transfer full): a new #CflMap; unref with g_object_unref()
 *
 * Since: 0.1.0
 */
CflMap *
cfl_map_new (void)
{
	return g_object_new (CFL_TYPE_MAP, NULL);
}
