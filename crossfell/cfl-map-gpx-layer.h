/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CFL_MAP_GPX_LAYER_H
#define CFL_MAP_GPX_LAYER_H

#include <glib.h>
#include <glib-object.h>
#include <champlain/champlain.h>
#include <libgpx/gpx.h>

G_BEGIN_DECLS

#define CFL_TYPE_MAP_GPX_LAYER		(cfl_map_gpx_layer_get_type ())
#define CFL_MAP_GPX_LAYER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), CFL_TYPE_MAP_GPX_LAYER, CflMapGpxLayer))
#define CFL_MAP_GPX_LAYER_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), CFL_TYPE_MAP_GPX_LAYER, CflMapGpxLayerClass))
#define CFL_IS_MAP_GPX_LAYER(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), CFL_TYPE_MAP_GPX_LAYER))
#define CFL_IS_MAP_GPX_LAYER_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), CFL_TYPE_MAP_GPX_LAYER))
#define CFL_MAP_GPX_LAYER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), CFL_TYPE_MAP_GPX_LAYER, CflMapGpxLayerClass))

typedef struct _CflMapGpxLayerPrivate	CflMapGpxLayerPrivate;

/**
 * CflMapGpxLayer:
 *
 * All the fields in the #CflMapGpxLayer structure are private and should never be accessed directly.
 *
 * Since: 0.1.0
 */
typedef struct {
	/*< private >*/
	ChamplainLayer parent;
	CflMapGpxLayerPrivate *priv;
} CflMapGpxLayer;

/**
 * CflMapGpxLayerClass:
 *
 * All of the fields in the #CflMapGpxLayerClass structure are private and should never be accessed directly.
 *
 * Since: 0.1.0
 */
typedef struct {
	/*< private >*/
	ChamplainLayerClass parent;
} CflMapGpxLayerClass;

GType cfl_map_gpx_layer_get_type (void) G_GNUC_CONST;

CflMapGpxLayer *cfl_map_gpx_layer_new (void) G_GNUC_MALLOC G_GNUC_WARN_UNUSED_RESULT;

GpxFile *cfl_map_gpx_layer_get_file (CflMapGpxLayer *self);
void cfl_map_gpx_layer_set_file (CflMapGpxLayer *self, GpxFile *gpx_file);

G_END_DECLS

#endif /* !CFL_MAP_GPX_LAYER_H */
