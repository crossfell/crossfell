/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:cfl-application
 * @short_description: main application
 * @stability: Unstable
 * @include: crossfell/cfl-application.h
 *
 * Main application class. TODO: More documentation.
 *
 * Since: 0.1.0
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <clutter-gtk/clutter-gtk.h>

#include "config.h"
#include "cfl-application.h"
#include "cfl-main-window.h"

static void cfl_application_startup (GApplication *app);
static void cfl_application_activate (GApplication *application);
static void cfl_application_open (GApplication  *application, GFile **files, gint n_files, const gchar *hint);

G_DEFINE_TYPE (CflApplication, cfl_application, GTK_TYPE_APPLICATION)

static void
cfl_application_class_init (CflApplicationClass *klass)
{
	GApplicationClass *application_class = G_APPLICATION_CLASS (klass);

	application_class->startup = cfl_application_startup;
	application_class->activate = cfl_application_activate;
	application_class->open = cfl_application_open;
}

static void
cfl_application_init (CflApplication *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, CFL_TYPE_APPLICATION, CflApplicationPrivate);

        bindtextdomain ("crossfell", LOCALE_DIR);
        textdomain ("crossfell");
        g_set_prgname ("crossfell");
        /* Translators: This is the application name. */
        g_set_application_name (_("Crossfell"));

	/* Set up some common properties. */
	g_application_set_application_id (G_APPLICATION (self), "org.gnome.Crossfell");
	g_application_set_flags (G_APPLICATION (self), G_APPLICATION_HANDLES_OPEN);
	g_object_set (G_OBJECT (self), "register-session", TRUE, NULL);
}

static void
cfl_application_activate (GApplication *application)
{
	CflMainWindow *window;

	/* Starting the program for the first time. Create the main window. */
	window = cfl_main_window_new (GTK_APPLICATION (application));
	gtk_widget_show (GTK_WIDGET (window));
}

static void
cfl_application_open (GApplication  *application, GFile **files, gint n_files, const gchar *hint)
{
	gint i;

	for (i = 0; i < n_files; i++) {
		g_message ("TODO: Implement opening files.");
	}
}

static CflMainWindow *
get_main_window (CflApplication *self)
{
	/* The main window is always ID 1. */
	GtkWindow *window = gtk_application_get_window_by_id (GTK_APPLICATION (self), 1);
	g_assert (CFL_IS_MAIN_WINDOW (window));
	return CFL_MAIN_WINDOW (window);
}

static void
new_trip_action_cb (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	GApplication *app = user_data;

	/* Start a new trip. */
	cfl_main_window_new_trip (get_main_window (CFL_APPLICATION (app)));
}

static void
about_action_cb (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	const gchar *authors[] = {
		"Philip Withnall <philip@tecnocode.co.uk>",
		NULL
	};

	gtk_show_about_dialog (NULL,
	                       /* Translators: This is the application name for the 'about' dialogue. */
	                       "title", _("About Crossfell"),
	                       "comments", _("Crossfell is a trip planning tool for outdoors enthusiasts."),
	                       "license-type", GTK_LICENSE_GPL_3_0,
	                       "authors", authors,
	                       "translator-credits", _("translator-credits"),
	                       "version", PACKAGE_VERSION,
	                       "website", PACKAGE_URL,
	                       NULL);
}

static void
quit_action_cb (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	GApplication *app = user_data;
	g_application_quit (app);
}

static GActionEntry app_entries[] = {
	{ "new-trip", new_trip_action_cb, NULL, NULL, NULL },
	{ "about", about_action_cb, NULL, NULL, NULL },
	{ "quit", quit_action_cb, NULL, NULL, NULL },
};

static void
cfl_application_startup (GApplication *app)
{
	GtkBuilder *builder;
	ClutterInitError status;

	/* Chain up. */
	G_APPLICATION_CLASS (cfl_application_parent_class)->startup (app);

	/* Set up Clutter. */
	status = gtk_clutter_init (NULL, NULL);
	if (status != CLUTTER_INIT_SUCCESS) {
		/* Error. */
		g_error ("Error %u initialising Clutter.", status);
		g_application_quit (app);
	}

	/* Add actions. */
	g_action_map_add_action_entries (G_ACTION_MAP (app), app_entries, G_N_ELEMENTS (app_entries), app);

	/* Add the app menu. */
	builder = gtk_builder_new ();
	gtk_builder_add_from_string (builder, /* TODO: move to GResource */
		"<interface>"
			"<menu id='app-menu'>"
				"<section>"
					"<item>"
						"<attribute name='label' translatable='yes'>_New Trip</attribute>"
						"<attribute name='action'>app.new-trip</attribute>"
						"<attribute name='accel'>&lt;Primary&gt;n</attribute>"
					"</item>"
				"</section>"
				"<section>"
					"<item>"
						"<attribute name='label' translatable='yes'>_About Crossfell</attribute>"
						"<attribute name='action'>app.about</attribute>"
					"</item>"
				"</section>"
				"<section>"
					"<item>"
						"<attribute name='label' translatable='yes'>_Quit</attribute>"
						"<attribute name='action'>app.quit</attribute>"
						"<attribute name='accel'>&lt;Primary&gt;q</attribute>"
					"</item>"
				"</section>"
			"</menu>"
			"<menu id='menubar'>"
				"<submenu>"
					"<attribute name='label' translatable='yes'>_View</attribute>"
					"<section>"
						"<item>"
							"<attribute name='label' translatable='yes'>_Fullscreen</attribute>"
							"<attribute name='action'>win.fullscreen</attribute>"
							"<attribute name='accel'>F11</attribute>"
						"</item>"
					"</section>"
				"</submenu>"
			"</menu>"
		"</interface>", -1, NULL);
	gtk_application_set_app_menu (GTK_APPLICATION (app), G_MENU_MODEL (gtk_builder_get_object (builder, "app-menu")));
	gtk_application_set_menubar (GTK_APPLICATION (app), G_MENU_MODEL (gtk_builder_get_object (builder, "menubar")));
	
	g_object_unref (builder);
}

/**
 * cfl_application_new:
 *
 * Creates a new #CflApplication with default properties.
 *
 * Return value: (transfer full): a new #CflApplication; unref with g_object_unref()
 *
 * Since: 0.1.0
 */
CflApplication *
cfl_application_new (void)
{
	return g_object_new (CFL_TYPE_APPLICATION, NULL);
}
