/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file is based on libchamplain's champlain-path-layer.c, LGPLv2.1+, by:
 *     Copyright (C) 2008-2009 Pierre-Luc Beaudoin <pierre-luc@pierlux.com>
 *     Copyright (C) 2011-2013 Jiri Techet <techet@gmail.com>
 */

/**
 * SECTION:cfl-map-gpx-layer
 * @short_description: #ChamplainLayer to display a libgpx track or route
 * @stability: Unstable
 * @include: crossfell/cfl-map-gpx-layer.h
 *
 * Main map_gpx_layer class. TODO: More documentation.
 *
 * Since: 0.1.0
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <champlain/champlain.h>
#include <libgpx/gpx.h>

#include "cfl-map-gpx-layer.h"

static void cfl_map_gpx_layer_dispose (GObject *object);
static void cfl_map_gpx_layer_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void cfl_map_gpx_layer_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);

static ChamplainBoundingBox *cfl_map_gpx_layer_get_bounding_box (ChamplainLayer *layer);
static void cfl_map_gpx_layer_set_view (ChamplainLayer *layer, ChamplainView *view);

static gboolean redraw_path (ClutterCanvas *canvas, cairo_t *cr, int width, int height, CflMapGpxLayer *self);

struct _CflMapGpxLayerPrivate {
	/* The GPX file we're rendering. */
	GpxFile *gpx_file;

	/* Champlain fluff. */
	ChamplainView *view;
	ClutterContent *canvas;
	ClutterActor *actor;
	gboolean redraw_scheduled;
};

enum {
	PROP_FILE = 1,
};

G_DEFINE_TYPE (CflMapGpxLayer, cfl_map_gpx_layer, CHAMPLAIN_TYPE_LAYER)

static void
cfl_map_gpx_layer_class_init (CflMapGpxLayerClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	ChamplainLayerClass *layer_class = CHAMPLAIN_LAYER_CLASS (klass);

	g_type_class_add_private (klass, sizeof (CflMapGpxLayerPrivate));

	gobject_class->get_property = cfl_map_gpx_layer_get_property;
	gobject_class->set_property = cfl_map_gpx_layer_set_property;
	gobject_class->dispose = cfl_map_gpx_layer_dispose;

	layer_class->set_view = cfl_map_gpx_layer_set_view;
	layer_class->get_bounding_box = cfl_map_gpx_layer_get_bounding_box;

	/* TODO: This should use a boxed type or object. */
	/**
	 * CflMapGpxLayer:file:
	 *
	 * #GpxFile to display tracks and routes from. All the tracks and all the routes in the file are
	 * displayed.
	 *
	 * Since: 0.1.0
	 */
	g_object_class_install_property (gobject_class, PROP_FILE,
	                                 g_param_spec_pointer ("file",
	                                                       "File", "GpxFile to display tracks and routes from.",
	                                                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
cfl_map_gpx_layer_init (CflMapGpxLayer *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, CFL_TYPE_MAP_GPX_LAYER, CflMapGpxLayerPrivate);

	/* Set up the Champlain fluff. */
	self->priv->view = NULL;

	self->priv->canvas = clutter_canvas_new ();
	clutter_canvas_set_size (CLUTTER_CANVAS (self->priv->canvas), 255, 255);
	g_signal_connect (self->priv->canvas, "draw", (GCallback) redraw_path, self);

	self->priv->actor = clutter_actor_new ();
	clutter_actor_set_size (self->priv->actor, 255, 255);
	clutter_actor_set_content (self->priv->actor, self->priv->canvas);
	clutter_actor_add_child (CLUTTER_ACTOR (self), self->priv->actor);
}

static void
cfl_map_gpx_layer_dispose (GObject *object)
{
	CflMapGpxLayer *self = CFL_MAP_GPX_LAYER (object);
	CflMapGpxLayerPrivate *priv = self->priv;

	cfl_map_gpx_layer_set_file (self, NULL);

	if (priv->view != NULL) {
		cfl_map_gpx_layer_set_view (CHAMPLAIN_LAYER (self), NULL);
	}

	g_clear_object (&priv->canvas);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (cfl_map_gpx_layer_parent_class)->dispose (object);
}

static void
cfl_map_gpx_layer_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
	CflMapGpxLayerPrivate *priv = CFL_MAP_GPX_LAYER (object)->priv;

	switch (property_id) {
		case PROP_FILE:
			if (priv->gpx_file != NULL) {
				gpx_file_ref (priv->gpx_file);
			}
			g_value_set_pointer (value, priv->gpx_file);
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
cfl_map_gpx_layer_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
	CflMapGpxLayer *self = CFL_MAP_GPX_LAYER (object);

	switch (property_id) {
		case PROP_FILE:
			cfl_map_gpx_layer_set_file (self, g_value_get_pointer (value));
			break;
		default:
			/* We don't have any other property... */
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
invalidate_canvas (CflMapGpxLayer *self)
{
	CflMapGpxLayerPrivate *priv = self->priv;
	gfloat width, height;

	width = 256;
	height = 256;
	if (priv->view != NULL) {
		clutter_actor_get_size (CLUTTER_ACTOR (priv->view), &width, &height);
	}

	clutter_canvas_set_size (CLUTTER_CANVAS (priv->canvas), width, height);
	clutter_actor_set_size (priv->actor, width, height);
	clutter_content_invalidate (priv->canvas);
	priv->redraw_scheduled = FALSE;
}

static void
schedule_redraw (CflMapGpxLayer *self)
{
	if (self->priv->redraw_scheduled == FALSE) {
		self->priv->redraw_scheduled = TRUE;
		g_idle_add_full (CLUTTER_PRIORITY_REDRAW, (GSourceFunc) invalidate_canvas,
		                 g_object_ref (self), (GDestroyNotify) g_object_unref);
	}
}

static void
relocate_cb (G_GNUC_UNUSED GObject *gobject, CflMapGpxLayer *self)
{
	g_return_if_fail (CFL_IS_MAP_GPX_LAYER (self));

	schedule_redraw (self);
}

static gboolean
redraw_path (ClutterCanvas *canvas, cairo_t *cr, int width, int height, CflMapGpxLayer *self)
{
	CflMapGpxLayerPrivate *priv = self->priv;
	ChamplainView *view = priv->view;
	gint x, y;
	unsigned int n_routes;
	const GpxRoute * const *routes;
	guint i;
  
	/* Layer hasn't been added to a view. */
	if (view == NULL) {
		return FALSE;
	}

	/* Redraw area is empty. */
	if (width == 0.0 || height == 0.0) {
		return FALSE;
	}

	/* Don't currently have a file to display. */
	if (priv->gpx_file == NULL) {
		return FALSE;
	}

	/* Centre the actor in the viewport. */
	champlain_view_get_viewport_origin (priv->view, &x, &y);
	clutter_actor_set_position (priv->actor, x, y);

	/* Clear the drawing area */
	cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint (cr);
	cairo_set_operator (cr, CAIRO_OPERATOR_OVER);

	cairo_set_line_join (cr, CAIRO_LINE_JOIN_BEVEL);

	/* Draw the path. */
	/* FIXME: This could be a lot cleverer. In conjunction with changes in libgpx, it should only
	 * retrieve the GPX points which are visible in the bounding box returned by
	 * champlain_view_get_bounding_box(). The returned set of GPX points should be simplified to
	 * a resolution appropriate to the current zoom level (champlain_view_get_zoom_level()).
	 *
	 * It also needs to support selective drawing of different routes and tracks in the file, and
	 * drawing them with different styles. */
	routes = gpx_file_get_routes (priv->gpx_file, &n_routes);

	for (i = 0; i < n_routes; i++) {
		guint j;
		unsigned int n_waypoints;
		const GpxWaypoint * const *waypoints = gpx_route_get_waypoints (routes[i], &n_waypoints);

		for (j = 0; j < n_waypoints; j++) {
			gfloat waypoint_x, waypoint_y;
			const GpxWaypoint *waypoint = waypoints[j];

			waypoint_x = champlain_view_longitude_to_x (view, waypoint->longitude);
			waypoint_y = champlain_view_latitude_to_y (view, waypoint->latitude);

			cairo_line_to (cr, waypoint_x, waypoint_y);
		}
	}

	cairo_set_source_rgba (cr, 0.0, 0.0, 0.0, 1.0);
	cairo_set_line_width (cr, 3.0);
	cairo_stroke (cr);

	return FALSE;
}

static void
redraw_path_cb (G_GNUC_UNUSED GObject *gobject, G_GNUC_UNUSED GParamSpec *arg1, CflMapGpxLayer *self)
{
	schedule_redraw (self);
}

static void
cfl_map_gpx_layer_set_view (ChamplainLayer *layer, ChamplainView *view)
{
	CflMapGpxLayer *self;

	g_return_if_fail (CFL_IS_MAP_GPX_LAYER (layer));
	g_return_if_fail (CHAMPLAIN_IS_VIEW (view) || view == NULL);

	self = CFL_MAP_GPX_LAYER (layer);

	if (view != NULL) {
		g_object_ref (view);
	}

	if (self->priv->view != NULL) {
		g_signal_handlers_disconnect_by_func (self->priv->view, (GCallback) relocate_cb, self);
		g_signal_handlers_disconnect_by_func (self->priv->view, (GCallback) redraw_path_cb, self);

		g_object_unref (self->priv->view);
	}

	self->priv->view = view;

	if (view != NULL) {
		g_signal_connect (view, "layer-relocated", (GCallback) relocate_cb, layer);
		g_signal_connect (view, "notify::latitude", (GCallback) redraw_path_cb, layer);
		g_signal_connect (view, "notify::zoom-level", (GCallback) redraw_path_cb, layer);

		schedule_redraw (self);
	}
}

static ChamplainBoundingBox *
cfl_map_gpx_layer_get_bounding_box (ChamplainLayer *layer)
{
	CflMapGpxLayerPrivate *priv = CFL_MAP_GPX_LAYER (layer)->priv;
	ChamplainBoundingBox *champlain_bbox;
	GpxBoundingBox gpx_bbox;

	/* Create a new Champlain bounding box. */
	champlain_bbox = champlain_bounding_box_new ();

	/* If we have a file, pinch its bounding box. */
	if (priv->gpx_file != NULL) {
		gpx_file_get_bounding_box (priv->gpx_file, &gpx_bbox);

		champlain_bbox->left = gpx_bbox.left;
		champlain_bbox->right = gpx_bbox.right;
		champlain_bbox->top = gpx_bbox.top;
		champlain_bbox->bottom = gpx_bbox.bottom;
	}

	/* Not allowed to return an empty bounding box. */
	if (champlain_bbox->left == champlain_bbox->right) {
		champlain_bbox->left -= 0.0001;
		champlain_bbox->right += 0.0001;
	}

	if (champlain_bbox->bottom == champlain_bbox->top) {
		champlain_bbox->bottom -= 0.0001;
		champlain_bbox->top += 0.0001;
	}

	return champlain_bbox;
}

/**
 * cfl_map_gpx_layer_new:
 *
 * Creates a new #CflMapGpxLayer with default properties.
 *
 * Return value: (transfer full): a new #CflMapGpxLayer; unref with g_object_unref()
 *
 * Since: 0.1.0
 */
CflMapGpxLayer *
cfl_map_gpx_layer_new (void)
{
	return g_object_new (CFL_TYPE_MAP_GPX_LAYER, NULL);
}

/**
 * cfl_map_gpx_layer_get_file:
 * @self: a #CflMapGpxLayer
 *
 * Get the value of the #CflMapGpxLayer:file property.
 *
 * Return value: (transfer none) (allow-none): the #GpxFile being displayed, or %NULL
 *
 * Since: 0.1.0
 */
GpxFile *
cfl_map_gpx_layer_get_file (CflMapGpxLayer *self)
{
	g_return_val_if_fail (CFL_IS_MAP_GPX_LAYER (self), NULL);
	return self->priv->gpx_file;
}

/**
 * cfl_map_gpx_layer_set_file:
 * @self: a #CflMapGpxLayer
 * @gpx_file: (allow-none) (transfer none): a new #GpxFile to display, or %NULL
 *
 * Set the value of the #CflMapGpxLayer:file property.
 *
 * Since: 0.1.0
 */
void
cfl_map_gpx_layer_set_file (CflMapGpxLayer *self, GpxFile *gpx_file)
{
	CflMapGpxLayerPrivate *priv;

	g_return_if_fail (CFL_IS_MAP_GPX_LAYER (self));

	priv = self->priv;

	if (priv->gpx_file == gpx_file) {
		return;
	}

	if (gpx_file != NULL) {
		gpx_file_ref (gpx_file);
	}

	if (priv->gpx_file != NULL) {
		gpx_file_unref (priv->gpx_file);
	}

	priv->gpx_file = gpx_file;
	schedule_redraw (self);
}
