/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * Crossfell
 * Copyright (C) Philip Withnall 2013 <philip@tecnocode.co.uk>
 *
 * Crossfell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crossfell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crossfell.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SECTION:cfl-main_window
 * @short_description: main application window
 * @stability: Unstable
 * @include: crossfell/cfl-main-window.h
 *
 * Main window of the application. TODO: Write more documentation.
 *
 * Since: 0.1.0
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <champlain-gtk/champlain-gtk.h>
#include <libgd/gd.h>

#include "cfl-main-window.h"
#include "cfl-map.h"

static void cfl_main_window_dispose (GObject *object);

static void set_up_window (CflMainWindow *self);

struct _CflMainWindowPrivate {
	GtkStack *stack;
	GtkListStore *main_view_model;
	ChamplainView *map_view;
};

G_DEFINE_TYPE (CflMainWindow, cfl_main_window, GTK_TYPE_APPLICATION_WINDOW)

static void
cfl_main_window_class_init (CflMainWindowClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (CflMainWindowPrivate));

	gobject_class->dispose = cfl_main_window_dispose;
}

static void
cfl_main_window_init (CflMainWindow *self)
{
	self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, CFL_TYPE_MAIN_WINDOW, CflMainWindowPrivate);

	/* Set up the child widgets. */
	set_up_window (self);
}

static void
cfl_main_window_dispose (GObject *object)
{
	CflMainWindowPrivate *priv = CFL_MAIN_WINDOW (object)->priv;

	g_clear_object (&priv->main_view_model);

	/* Chain up to the parent class */
	G_OBJECT_CLASS (cfl_main_window_parent_class)->dispose (object);
}

static void
main_view_item_activated_cb (GdMainView *main_view, const gchar *item_id, const GtkTreePath *item_path, CflMainWindow *self)
{
	/* Open the trip. */
	cfl_main_window_open_trip (self, item_id);
}

static void
activate_toggle_cb (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	GVariant *state;

	state = g_action_get_state (G_ACTION (action));
	g_action_change_state (G_ACTION (action), g_variant_new_boolean (!g_variant_get_boolean (state)));
	g_variant_unref (state);
}

static void
change_fullscreen_state_cb (GSimpleAction *action, GVariant *state, gpointer user_data)
{
	GtkWindow *window = GTK_WINDOW (user_data);

	if (g_variant_get_boolean (state)) {
		gtk_window_fullscreen (window);
	} else {
		gtk_window_unfullscreen (window);
	}

	g_simple_action_set_state (action, state);
}


static GActionEntry action_map_entries[] = {
	{ "fullscreen", activate_toggle_cb, NULL, "false", change_fullscreen_state_cb },
};

static void
set_up_window (CflMainWindow *self)
{
	CflMainWindowPrivate *priv = self->priv;
	GtkWidget *grid, *toolbar, *button, *box, *label, *sw, *main_view, *map;

	gtk_window_set_default_size (GTK_WINDOW (self), 800, 600);
	/* Translators: This is the application name. */
	gtk_window_set_title (GTK_WINDOW (self), _("Crossfell"));

	/* Actions. */
	g_action_map_add_action_entries (G_ACTION_MAP (self), action_map_entries, G_N_ELEMENTS (action_map_entries), self);

	/* Wrap everything in a grid. */
	grid = gtk_grid_new ();
	gtk_container_add (GTK_CONTAINER (self), grid);

	/* Toolbar. */
	toolbar = gtk_toolbar_new ();
	gtk_grid_attach (GTK_GRID (grid), toolbar, 0, 0, 1, 1);

	/* Fullscreen button. */
	button = gtk_tool_item_new ();
	box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);
	gtk_container_add (GTK_CONTAINER (button), box);
	label = gtk_label_new (_("Fullscreen:"));
	gtk_container_add (GTK_CONTAINER (box), label);
	sw = gtk_switch_new ();
	gtk_actionable_set_action_name (GTK_ACTIONABLE (sw), "win.fullscreen");
	gtk_container_add (GTK_CONTAINER (box), sw);
	gtk_container_add (GTK_CONTAINER (toolbar), GTK_WIDGET (button));

	/* Main libgd stack. */
	priv->stack = GTK_STACK (gtk_stack_new ());
	gtk_grid_attach (GTK_GRID (grid), GTK_WIDGET (priv->stack), 0, 1, 1, 1);

	/* Add the main view which lists existing journeys. */
	main_view = GTK_WIDGET (gd_main_view_new (GD_MAIN_VIEW_ICON));
	priv->main_view_model =
		gtk_list_store_new (8, /* this follows GdMainColumns */
		                    G_TYPE_STRING,
		                    G_TYPE_STRING,
		                    G_TYPE_STRING,
		                    G_TYPE_STRING,
		                    GDK_TYPE_PIXBUF,
		                    G_TYPE_LONG,
		                    G_TYPE_BOOLEAN,
		                    G_TYPE_UINT);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (priv->main_view_model),
	                                      GD_MAIN_COLUMN_MTIME, GTK_SORT_DESCENDING);
	gd_main_view_set_model (GD_MAIN_VIEW (main_view), GTK_TREE_MODEL (priv->main_view_model));
	gtk_stack_add_named (GTK_STACK (priv->stack), main_view, "main-view");

	g_signal_connect (G_OBJECT (main_view), "item-activated",
	                  (GCallback) main_view_item_activated_cb, self);

	/* And add the map view which displays a specific journey. */
	map = GTK_WIDGET (cfl_map_new ());
	priv->map_view = gtk_champlain_embed_get_view (GTK_CHAMPLAIN_EMBED (map));
	gtk_stack_add_named (GTK_STACK (priv->stack), map, "map-view");

	/* Done. It's up to the caller to show the window itself. */
	gtk_widget_show_all (GTK_WIDGET (grid));
}

/**
 * cfl_main_window_new:
 *
 * Creates a new #CflMainWindow with default properties and adds it to the given @application as a window.
 *
 * Return value: (transfer full): a new #CflMainWindow; unref with g_object_unref()
 *
 * Since: 0.1.0
 */
CflMainWindow *
cfl_main_window_new (GtkApplication *application)
{
	return g_object_new (CFL_TYPE_MAIN_WINDOW, "application", application, NULL);
}

/**
 * cfl_main_window_new_trip:
 * @self: a #CflMainWindow
 *
 * Starts a new trip.
 *
 * Since: 0.1.0
 */
void
cfl_main_window_new_trip (CflMainWindow *self)
{
	g_return_if_fail (CFL_IS_MAIN_WINDOW (self));

	/* Switch to the map view. */
	gtk_stack_set_visible_child_name (self->priv->stack, "map-view");
}

/**
 * cfl_main_window_open_trip:
 * @self: a #CflMainWindow
 * @id: the trip ID
 *
 * Opens an existing trip named @id.
 *
 * Since: 0.1.0
 */
void
cfl_main_window_open_trip (CflMainWindow *self, const gchar *id)
{
	g_return_if_fail (CFL_IS_MAIN_WINDOW (self));

	/* TODO */

	/* Switch to the map view. */
	gtk_stack_set_visible_child_name (self->priv->stack, "map-view");
}
